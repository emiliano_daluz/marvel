//
//  marvelUITests.swift
//  marvelUITests
//
//  Created by Emiliano Da Luz on 21/01/2022.
//

import XCTest

class marvelUITests: XCTestCase {

    var app: XCUIApplication!

    override func setUpWithError() throws {
        try super.setUpWithError()
        continueAfterFailure = false
        app = XCUIApplication()
        app.launch()
    }

    func testMainFlow() {
        
        let elementsQuery = app.scrollViews.otherElements
        let button = elementsQuery.buttons["MARVEL CHARACTERS >"]
        
        XCTAssertNotNil(elementsQuery.buttons["MARVEL COMICS >"])
        
        button.tap()
        
        app.collectionViews.children(matching: .cell).element(boundBy: 0).children(matching: .other).element.tap()
        elementsQuery.staticTexts["None Description"].swipeLeft()
        
        XCTAssertNotNil(elementsQuery.staticTexts["Description"])
        XCTAssertEqual(app.collectionViews.children(matching: .cell).count, 3)
        
        app.terminate()
    }
}
