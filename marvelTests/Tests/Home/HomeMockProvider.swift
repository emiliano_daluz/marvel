//
//  HomeMockProvider.swift
//  marvelTests
//
//  Created by Emiliano Da Luz on 26/01/2022.
//

import Foundation
@testable import marvel

class HomeMockProvider: HomeProviderProtocol {
    func getCharacters(result: @escaping (Result<MarvelResponse<Character>, APIServiceError>) -> Void) {
        guard let response: MarvelResponse<Character> = TestUtils.parseJSON("getCharactersMock.json") else {
            result(.failure(.invalidResponse))
            return
        }
        
        result(.success(response))
    }
    
    func getComics(result: @escaping (Result<MarvelResponse<Comic>, APIServiceError>) -> Void) {
        let response = MarvelResponse<Comic>(attributionText: "", data: nil)
        return result(.success(response))
    }
    
    
}
