//
//  HomeTest.swift
//  marvelTests
//
//  Created by Emiliano Da Luz on 26/01/2022.
//

import XCTest
@testable import marvel

class HomeTest: XCTestCase {
    
    /*This test inject the Mock to verify if the main flow is working well
        calls sequence: HomePresenter -> HomeInteractor -> MockProvider -> Mock
     */
    func testHomeFlow() {
        let interactor = HomeInteractor(provider: HomeMockProvider())
        let presenter = HomePresenter(wireframe: HomeWireframe(), interactor: interactor)
 
        presenter.getCharacters { response in
            switch response {
                case .success(let response):
                
                //Verify if the parsing was success
                XCTAssertEqual(response.attributionText, "Data provided by Marvel. © 2022 MARVEL")
                XCTAssertTrue(response.data?.count == 4, "Get Characters parsing success")
                XCTAssertTrue(response.data?.total == 21, "Response total field is OK")
                XCTAssertTrue(response.data?.limit == 4, "Response limit field is OK")
                
                //Verify First Character
                guard let firstCharacter = response.data?.results?.first else {
                    XCTFail("Get Characters parsing has failed")
                    return
                }
                
                XCTAssertEqual(firstCharacter.id, 1010727)
                XCTAssertNotNil(firstCharacter.description)

                case .failure(_):
                XCTFail("Get Characters parsing has failed")
            }
        }
        
    }
}
