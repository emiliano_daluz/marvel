//
//  MarvelStyles.swift
//  marvel
//
//  Created by Emiliano Da Luz on 23/01/2022.
//

import Foundation

public final class MarvelStyles {
    
    // MARK: - Color palette
    public static let primary = MarvelPrimaryColor()
    public static let secondary = MarvelSecondaryColor()
    public static let shadow = MarvelShadowColor()
    
    // MARK: - Font
    public static let font = MarvelFontCore()
}
