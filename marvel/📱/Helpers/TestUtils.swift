//
//  TestUtils.swift
//  marvel
//
//  Created by Emiliano Da Luz on 26/01/2022.
//

import Foundation
import UIKit

class TestUtils {
    static func parseJSONAsData(_ filename: String) -> Data? {
        guard let file = Bundle.main.url(forResource: filename, withExtension: nil)
        else {
            print("Parse Mock JSON - Couldn't find \(filename) in main bundle.")
            return nil
        }
        
        let data: Data
        do {
            data = try Data(contentsOf: file)
        } catch {
            print("Parse Mock JSON - Couldn't load \(filename) from main bundle:\n\(error)")
            return nil
        }
        return data
    }

    static func parseJSON<T: Decodable>(_ filename: String) -> T? {
        guard let data: Data = parseJSONAsData(filename) else { return nil }

        do {
            return try JSONDecoder().decode(T.self, from: data)
        } catch {
            print("Parse Mock JSON - Couldn't parse \(filename) as \(T.self):\n\(error)")
            return nil
        }
    }
    
    static func parseJSONToDict(_ filename: String) -> [String: Any]? {
        guard let file = Bundle.main.url(forResource: filename, withExtension: nil)
        else {
            print("Couldn't find \(filename) in main bundle.")
            return nil
        }
        
        let data: Data
        do {
            data = try Data(contentsOf: file)
        } catch {
            print("Couldn't load \(filename) from main bundle:\n\(error)")
            return nil
        }
        
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
            print("Couldn't parse \(filename)")
            return nil
        }
    }
    
}
