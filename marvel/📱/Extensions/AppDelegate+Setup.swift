//
//  AppDelegate+Setup.swift
//  marvel
//
//  Created by Emiliano Da Luz on 22/01/2022.
//

import Foundation
import UIKit

extension AppDelegate {
    
    func setupRootController() {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        
        let navigationController = UINavigationController()
        window?.rootViewController = navigationController
        
        HomeModule().showHome(navigation: navigationController)
    }
}
