//
//  UIImageView+URL.swift
//  marvel
//
//  Created by Emiliano Da Luz on 23/01/2022.
//

import Foundation
import UIKit

extension UIImageView {
    func loadImage(at url: URL, placeholder: UIImage? = nil) {
      UIImageLoader.loader.load(url, for: self, placeholder: placeholder)
  }

  func cancelImageLoad() {
    UIImageLoader.loader.cancel(for: self)
  }
}
