//
//  ItemDetailsModule.swift
//  marvel
//
//  Created by Emiliano Da Luz on 23/01/2022.
//

import Foundation
import UIKit

class ItemDetailsModule {
    private let presenter: ItemDetailsPresenterTestable
    private let wireframe: ItemDetailsWireframeTestable
    private let interactor: ItemDetailsInteractorTestable
    
    init(item: HomeItem) {
        self.wireframe = ItemDetailsWireframe()
        self.interactor = ItemDetailsInteractor(item: item)
        self.presenter = ItemDetailsPresenter(wireframe: self.wireframe, interactor: self.interactor)
    }
    
    func showItemDetails(navigation: UINavigationController) {
        presenter.showItemDetails(navigation: navigation)
    }
}
