//
//  ItemDetailsPresenter.swift
//  marvel
//
//  Created by Emiliano Da Luz on 23/01/2022.
//

import Foundation
import UIKit

protocol ItemDetailsPresenterTestable {
    var wireframe: ItemDetailsWireframeTestable { get }
    var interactor: ItemDetailsInteractorTestable { get }
    
    func showItemDetails(navigation: UINavigationController)
    func showItemDetails(navigation: UINavigationController, item: HomeItem)
    func getItem() -> HomeItem
    func getSubitems(result: @escaping (Result<[HomeItem], APIServiceError>) -> Void)
}

class ItemDetailsPresenter: ItemDetailsPresenterTestable {
    var wireframe: ItemDetailsWireframeTestable
    var interactor: ItemDetailsInteractorTestable
    
    init(wireframe: ItemDetailsWireframeTestable, interactor: ItemDetailsInteractorTestable) {
        self.wireframe = wireframe
        self.interactor = interactor
    }
    
    //Transitions
    func showItemDetails(navigation: UINavigationController) {
        wireframe.showItemDetails(navigation: navigation, presenter: self)
    }
    
    func showItemDetails(navigation: UINavigationController, item: HomeItem) {
        wireframe.showItemDetails(navigation: navigation, item: item)
    }
    
    //Data
    func getItem() -> HomeItem {
        return interactor.getItem()
    }
    
    func getSubitems(result: @escaping (Result<[HomeItem], APIServiceError>) -> Void) {
        let item = getItem()
        guard let itemID = item.id else { return }
        
        //Get Comics for character
        if item is Character {
            interactor.getComics(characterID: itemID) { response in
                switch response {
                case .success(let responseData):
                    result(.success(responseData.data?.results ?? []))
                case .failure(let error):
                    result(.failure(error))
                }
            }
        } else {
            //Get Characters for Comic
            interactor.getCharacters(comicID: itemID) { response in
                switch response {
                case .success(let responseData):
                    result(.success(responseData.data?.results ?? []))
                case .failure(let error):
                    result(.failure(error))
                }
            }
        }
    }
}

