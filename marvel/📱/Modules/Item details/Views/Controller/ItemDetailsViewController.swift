//
//  ItemDetailsViewController.swift
//  marvel
//
//  Created by Emiliano Da Luz on 23/01/2022.
//

import UIKit

class ItemDetailsViewController: UIViewController {
    
    private enum Constant {
        static let multipleNumberOfLines: Int = Constant.multipleNumberOfLines
        static let placeholder: UIImage = UIImage(named: "placeholder") ?? UIImage()
    }

    @IBOutlet private var imageView: UIImageView! {
        didSet {
            imageView.contentMode = .scaleAspectFit
            imageView.backgroundColor = MarvelStyles.secondary.black
        }
    }
    
    @IBOutlet private var itemTitleLabel: UILabel! {
        didSet {
            itemTitleLabel.font = MarvelStyles.font.headlineBold
            itemTitleLabel.textColor = MarvelStyles.secondary.black
            itemTitleLabel.numberOfLines = Constant.multipleNumberOfLines
        }
    }
    
    @IBOutlet private var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.font = MarvelStyles.font.bodySemiBold
            descriptionLabel.textColor = MarvelStyles.secondary.black
            descriptionLabel.text = LanguageString.description
        }
    }
    
    @IBOutlet private var itemDescriptionLabel: UILabel! {
        didSet {
            itemDescriptionLabel.font = MarvelStyles.font.bodyRegular
            itemDescriptionLabel.textColor = MarvelStyles.secondary.black
            itemDescriptionLabel.numberOfLines = Constant.multipleNumberOfLines
        }
    }
    
    @IBOutlet private var collectionTitleLabel: UILabel! {
        didSet {
            collectionTitleLabel.text = ""
            collectionTitleLabel.font = MarvelStyles.font.bodySemiBold
        }
    }
    
    @IBOutlet private var bottomContainerView: UIView!
    private var bottomList: HorizontalCollectionView = HorizontalCollectionView()
    private var presenter: ItemDetailsPresenterTestable?
    
    //MARK: Init
    init(with presenter: ItemDetailsPresenterTestable) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews() {
        //Bottom List
        bottomList.delegate = self
        bottomList.translatesAutoresizingMaskIntoConstraints = false
        bottomContainerView.addSubview(bottomList)
        bottomList.topAnchor.constraint(equalTo: bottomContainerView.topAnchor).isActive = true
        bottomList.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        bottomList.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        bottomList.bottomAnchor.constraint(equalTo: bottomContainerView.bottomAnchor).isActive = true
        
        //Load Data
        getItem()
        getSubitems()
    }
    
    private func getItem() {
        let item = presenter?.getItem()
        
        //Screen Title
        switch item {
        case is Character:
            title = LanguageString.character
        case is Comic:
            title = LanguageString.comic
        default: break
        }
        
        //Screen Data
        itemTitleLabel.text = item?.title?.uppercased()
        itemDescriptionLabel.text = !(item?.description?.isEmpty ?? true) ? item?.description : LanguageString.emptyDescription
        
        guard let thumbnail = item?.thumbnail, let imageURL = ImageHelper.getImageURL(thumbnail: thumbnail) else { return }
        imageView.loadImage(at: imageURL, placeholder: Constant.placeholder)
    }
    
    /*
     IF the item is a Character gets the Comics for that Character
     ELSE IF the item is a Comic gets the Character for that Comic
     */
    private func getSubitems() {
        bottomList.startAnimating()
        presenter?.getSubitems(result: { [weak self] result in
            guard let self = self else { return }
            
            switch result {
                case .success(let items):
                DispatchQueue.main.async {
                    self.bottomList.stopAnimating()
                    
                    //Don't show bottom list if there are not subitems to show
                    guard !items.isEmpty else { return }
                    
                    self.setBottomListTitle()
                    self.bottomList.setData(items)
                }
                
                case .failure(let error):
                    print("Items Details" + error.localizedDescription)
            }
        })
    }
    
    private func setBottomListTitle() {
        let item = presenter?.getItem()
        
        self.bottomList.title = item is Character ? LanguageString.marvelComics : LanguageString.characterList
    }
}

extension ItemDetailsViewController: HorizontalCollectionViewDelegate {
    func headerTapped(horizontalCollectionView: HorizontalCollectionView) {
        //TODO: show details
    }
    
    func retryButtonTapped(horizontalCollectionView: HorizontalCollectionView) {
        //show empty state if it's needed on Item details
    }
    
    func didSelectItem(item: HomeItem) {
        guard let navigation = navigationController else { return }
        presenter?.showItemDetails(navigation: navigation, item: item)
    }
}
