//
//  ItemDetailsWireframe.swift
//  marvel
//
//  Created by Emiliano Da Luz on 23/01/2022.
//

import Foundation
import UIKit

protocol ItemDetailsWireframeTestable {
    func showItemDetails(navigation: UINavigationController, presenter: ItemDetailsPresenterTestable)
    func showItemDetails(navigation: UINavigationController, item: HomeItem)
}

class ItemDetailsWireframe: ItemDetailsWireframeTestable {
    func showItemDetails(navigation: UINavigationController, presenter: ItemDetailsPresenterTestable) {
        let detailsVC = ItemDetailsViewController(with: presenter)
        navigation.pushViewController(detailsVC, animated: true)
    }
    
    func showItemDetails(navigation: UINavigationController, item: HomeItem) {
        let itemDetails = ItemDetailsModule(item: item)
        itemDetails.showItemDetails(navigation: navigation)
    }
}
