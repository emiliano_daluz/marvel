//
//  ItemDetailsProvider.swift
//  marvel
//
//  Created by Emiliano Da Luz on 24/01/2022.
//

import Foundation

protocol ItemDetailsProviderProtocol {
    func getComics(characterID: Int, result: @escaping (Result<MarvelResponse<Comic>, APIServiceError>) -> Void)
    func getCharacters(comicID: Int, result: @escaping (Result<MarvelResponse<Character>, APIServiceError>) -> Void)
}

class ItemDetailsProvider: ItemDetailsProviderProtocol {
    func getComics(characterID: Int, result: @escaping (Result<MarvelResponse<Comic>, APIServiceError>) -> Void) {
        let url = MarvelAPIClient.baseURL + "/characters/" + "\(characterID)" + "/comics"
        
        guard let comicsURL = URL(string: url) else { return }
        
        return MarvelAPIClient.fetchResources(url: comicsURL, completion: result)
    }
    
    func getCharacters(comicID: Int, result: @escaping (Result<MarvelResponse<Character>, APIServiceError>) -> Void) {
        let url = MarvelAPIClient.baseURL + "/comics/" + "\(comicID)" + "/characters"
        
        guard let comicsURL = URL(string: url) else { return }
        
        return MarvelAPIClient.fetchResources(url: comicsURL, completion: result)
    }
}
