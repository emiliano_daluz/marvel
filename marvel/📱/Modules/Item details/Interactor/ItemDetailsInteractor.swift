//
//  ItemDetailsInteractor.swift
//  marvel
//
//  Created by Emiliano Da Luz on 23/01/2022.
//

import Foundation

protocol ItemDetailsInteractorTestable {
    func getItem() -> HomeItem
    func getComics(characterID: Int, result: @escaping (Result<MarvelResponse<Comic>, APIServiceError>) -> Void)
    func getCharacters(comicID: Int, result: @escaping (Result<MarvelResponse<Character>, APIServiceError>) -> Void)
}

class ItemDetailsInteractor: ItemDetailsInteractorTestable {
    private let item: HomeItem
    private var provider: ItemDetailsProviderProtocol
    
    init(item: HomeItem, provider: ItemDetailsProviderProtocol = ItemDetailsProvider()) {
        self.item = item
        self.provider = provider
    }
    
    func getItem() -> HomeItem {
        return item
    }
    
    func getComics(characterID: Int, result: @escaping (Result<MarvelResponse<Comic>, APIServiceError>) -> Void) {
        return provider.getComics(characterID: characterID, result: result)
    }
    
    func getCharacters(comicID: Int, result: @escaping (Result<MarvelResponse<Character>, APIServiceError>) -> Void) {
        return provider.getCharacters(comicID: comicID, result: result)
    }
}
