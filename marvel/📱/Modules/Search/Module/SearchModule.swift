//
//  SearchModule.swift
//  marvel
//
//  Created by Emiliano Da Luz on 24/01/2022.
//

import Foundation
import UIKit

class SearchModule {
    private let presenter: SearchPresenterTestable
    private let wireframe: SearchWireframeTestable
    private let interactor: SearchInteractorTestable
    
    init(itemType: MarvelItem) {
        self.wireframe = SearchWireframe()
        self.interactor = SearchInteractor()
        self.presenter = SearchPresenter(wireframe: self.wireframe, interactor: self.interactor, itemType: itemType)
    }
    
    func showSearch(navigation: UINavigationController) {
        presenter.showSearch(navigation: navigation)
    }
}
