//
//  SearchWireframe.swift
//  marvel
//
//  Created by Emiliano Da Luz on 24/01/2022.
//

import Foundation
import UIKit

protocol SearchWireframeTestable {
    func showSearch(navigation: UINavigationController, presenter: SearchPresenterTestable)
    func showItemDetails(navigation: UINavigationController, item: HomeItem)
}

class SearchWireframe: SearchWireframeTestable {
    func showSearch(navigation: UINavigationController, presenter: SearchPresenterTestable) {
        let searchVC = SearchViewController(with: presenter)
        navigation.pushViewController(searchVC, animated: true)
    }
    
    func showItemDetails(navigation: UINavigationController, item: HomeItem) {
        let itemDetails = ItemDetailsModule(item: item)
        itemDetails.showItemDetails(navigation: navigation)
    }
}
