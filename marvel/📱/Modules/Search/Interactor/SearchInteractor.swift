//
//  SearchInteractor.swift
//  marvel
//
//  Created by Emiliano Da Luz on 24/01/2022.
//

import Foundation

protocol SearchInteractorTestable {
    var pagination: Pagination? { get set }
    func searchCharacters(isFilter1Enabled: Bool, isFilter2Enabled: Bool, searchKey: String, result: @escaping (Result<MarvelResponse<Character>, APIServiceError>) -> Void)
    func searchComics(isFilter1Enabled: Bool, isFilter2Enabled: Bool, searchKey: String, result: @escaping (Result<MarvelResponse<Comic>, APIServiceError>) -> Void)
}

class SearchInteractor: SearchInteractorTestable {
    
    private enum Constant {
        static let characterSearchKey: String = "nameStartsWith"
        static let comicSearchKey: String = "titleStartsWith"
        static let limitKey: String = "limit"
        static let offsetKey: String = "offset"
    }
    
    private var provider: SearchProviderProtocol
    var pagination: Pagination?

    init(provider: SearchProviderProtocol = SearchProvider()) {
        self.provider = provider
    }
    
    func searchCharacters(isFilter1Enabled: Bool, isFilter2Enabled: Bool, searchKey: String, result: @escaping (Result<MarvelResponse<Character>, APIServiceError>) -> Void) {
        
        var params: [String: Any] = !searchKey.isEmpty ? [Constant.characterSearchKey: searchKey] : [:]
        
        //Pagination
        if let limit = pagination?.limit, let offset = pagination?.offset,
           let count = pagination?.count {
            params[Constant.limitKey] = limit
            params[Constant.offsetKey] = offset + count
        } else {
            params[Constant.limitKey] = 10
        }
        
        //Filters
        if isFilter1Enabled {
            params["orderBy"] = "name"
        }
        
        if isFilter2Enabled {
            params["orderBy"] = "modified"
        }
        
        return provider.searchCharacters(params: params, result: result)
    }
    
    func searchComics(isFilter1Enabled: Bool, isFilter2Enabled: Bool, searchKey: String, result: @escaping (Result<MarvelResponse<Comic>, APIServiceError>) -> Void) {
        var params: [String: Any] = !searchKey.isEmpty ? [Constant.comicSearchKey: searchKey] : [:]
        
        if isFilter1Enabled {
            params["orderBy"] = "title"
        }
        
        if isFilter2Enabled {
            params["orderBy"] = "onsaleDate"
        }
        
        return provider.searchComics(params: params, result: result)
    }
}
