//
//  SearchProvider.swift
//  marvel
//
//  Created by Emiliano Da Luz on 24/01/2022.
//

import Foundation

protocol SearchProviderProtocol {
    func searchCharacters(params: [String: Any], result: @escaping (Result<MarvelResponse<Character>, APIServiceError>) -> Void)
    func searchComics(params: [String: Any], result: @escaping (Result<MarvelResponse<Comic>, APIServiceError>) -> Void)
}

class SearchProvider: SearchProviderProtocol {
    
    private enum Domain {
        static let getCharacters = MarvelAPIClient.baseURL + "/characters"
        static let getComics = MarvelAPIClient.baseURL + "/comics"
    }
    
    func searchCharacters(params: [String: Any], result: @escaping (Result<MarvelResponse<Character>, APIServiceError>) -> Void) {
        guard let url = URL(string: Domain.getCharacters) else { return }
       return MarvelAPIClient.fetchResources(url: url, params: params, completion: result)
    }

    func searchComics(params: [String: Any], result: @escaping (Result<MarvelResponse<Comic>, APIServiceError>) -> Void) {
        guard let url = URL(string: Domain.getComics) else { return }
       return MarvelAPIClient.fetchResources(url: url, params: params, completion: result)
    }
}
