//
//  SearchPresenter.swift
//  marvel
//
//  Created by Emiliano Da Luz on 24/01/2022.
//

import Foundation
import UIKit

protocol SearchPresenterTestable {
    func getItemType() -> MarvelItem
    func showSearch(navigation: UINavigationController)
    func showItemDetails(navigation: UINavigationController, item: HomeItem)
    func searchItems(isFilter1Enabled: Bool, isFilter2Enabled: Bool, searchKey: String, result: @escaping (Result<[HomeItem], APIServiceError>) -> Void)
    func getControllerTitle() -> String
    func isPaginationLimit() -> Bool
    func searchHasChanged(isFilter1Enabled: Bool, isFilter2Enabled: Bool, searchKey: String) -> Bool
}

class SearchPresenter: SearchPresenterTestable {
    private var wireframe: SearchWireframeTestable
    private var interactor: SearchInteractorTestable
    
    var itemType: MarvelItem
    var items: [HomeItem] = []
    var lastSearch: String = ""
    
    ///These filter values are used to allow first call taking in account the values presented to the user in SearchViewController
    var lastFirstFilterValue: Bool = false
    var lastSecondFilterValue: Bool = true
    
    init(wireframe: SearchWireframeTestable, interactor: SearchInteractorTestable, itemType: MarvelItem) {
        self.wireframe = wireframe
        self.interactor = interactor
        self.itemType = itemType
    }
    
    func getControllerTitle() -> String {
        switch itemType {
        case .character:
            return LanguageString.character
        case .comic:
            return LanguageString.comic
        }
    }
    
    func getItemType() -> MarvelItem {
        return itemType
    }
    
    //Transitions
    func showSearch(navigation: UINavigationController) {
        wireframe.showSearch(navigation: navigation, presenter: self)
    }
    
    func showItemDetails(navigation: UINavigationController, item: HomeItem) {
        wireframe.showItemDetails(navigation: navigation, item: item)
    }
    
    //Backend Data
    func isPaginationLimit() -> Bool {
        guard let pagination = interactor.pagination, let offset = pagination.offset, let total = pagination.total else { return false }
        
        return offset == total
    }
    
    func searchItems(isFilter1Enabled: Bool,
                     isFilter2Enabled: Bool,
                     searchKey: String,
                     result: @escaping (Result<[HomeItem], APIServiceError>) -> Void) {
        
        //Reset Search params when searchKey or filters changed
        if searchHasChanged(isFilter1Enabled: isFilter1Enabled, isFilter2Enabled: isFilter2Enabled, searchKey: searchKey) {
            items = []
            lastSearch = searchKey
            lastFirstFilterValue = isFilter1Enabled
            lastSecondFilterValue = isFilter2Enabled
            interactor.pagination = nil
        }
        
        switch itemType {
        case .character:
            interactor.searchCharacters(isFilter1Enabled: isFilter1Enabled,
                                        isFilter2Enabled: isFilter2Enabled,
                                        searchKey: searchKey) { [weak self] response in
                switch response {
                case .success(let responseData):
                    self?.interactor.pagination = responseData.data
                    self?.items.append(contentsOf: responseData.data?.results ?? [])
                    result(.success(self?.items ?? []))
                case .failure(let error):
                    result(.failure(error))
                }
            }
        case .comic:
            interactor.searchComics(isFilter1Enabled: isFilter1Enabled,
                                    isFilter2Enabled: isFilter2Enabled,
                                    searchKey: searchKey) { [weak self] response in
                guard let self = self else { return }
                
                switch response {
                case .success(let responseData):
                    self.interactor.pagination = responseData.data
                    self.items.append(contentsOf: responseData.data?.results ?? [])
                    result(.success(self.items))
                case .failure(let error):
                    result(.failure(error))
                }
            }
        }
    }
    
    //Verifies if the search has changed checking searchKey and Filters
    func searchHasChanged(isFilter1Enabled: Bool, isFilter2Enabled: Bool, searchKey: String) -> Bool {
        return lastSearch != searchKey || lastFirstFilterValue != isFilter1Enabled || lastSecondFilterValue != isFilter2Enabled
    }
}

