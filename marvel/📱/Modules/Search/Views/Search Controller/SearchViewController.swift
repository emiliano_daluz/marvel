//
//  SearchViewController.swift
//  marvel
//
//  Created by Emiliano Da Luz on 24/01/2022.
//

import UIKit

class SearchViewController: UIViewController {
    
    private enum Constant {
        static let imageName = UIImage(named: "spiderMan")
        static let emptyTitleLabelLines = 2
    }

    @IBOutlet private var searchBar: UISearchBar!
    @IBOutlet private var collectionView: UICollectionView!
    
    //Filters
    @IBOutlet private var filter1Switch: UISwitch!
    @IBOutlet private var filter1Label: UILabel!
    @IBOutlet private var filter2Switch: UISwitch!
    @IBOutlet private var filter2Label: UILabel!

    //Empty View
    @IBOutlet private var emptyView: UIView!
    @IBOutlet private var emptyImageView: UIImageView!
    @IBOutlet private var emptyTitleLabel: UILabel!
    
    @IBOutlet private var activityIndicator: UIActivityIndicatorView!
    
    private var presenter: SearchPresenterTestable?
    
    private lazy var dataSource: VerticalCollectionViewDataSource = {
        let dataSource = VerticalCollectionViewDataSource()
        dataSource.delegate = self
        return dataSource
    }()
    
    //MARK: Init
    init(with presenter: SearchPresenterTestable) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    //MARK: Lyfe Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
        searchData(key: "")
    }
    
    private func setupSubviews() {
        searchBar.delegate = self
        title = presenter?.getControllerTitle()
        
        //Collection View
        view.backgroundColor = MarvelStyles.secondary.black
        collectionView.backgroundColor = MarvelStyles.secondary.black
        dataSource.collectionView = collectionView
        
        //Activity Indicator
        activityIndicator.isHidden = true
        activityIndicator.color = MarvelStyles.secondary.white
        
        //Empty View
        emptyView.isHidden = true
        emptyImageView.image = Constant.imageName
        emptyTitleLabel.text = LanguageString.emptyViewTitle
        emptyTitleLabel.font = MarvelStyles.font.captionOneSemiBold
        emptyTitleLabel.numberOfLines = Constant.emptyTitleLabelLines
        
        //Switchs
        
        
        filter2Switch.isOn = false
        
        let item = presenter?.getItemType()
        filter1Label.text = item == .character ? LanguageString.name : LanguageString.title
        filter2Label.text = item == .character ? LanguageString.modified : LanguageString.publicationDate
        
        filter1Label.textColor = MarvelStyles.secondary.white
        filter2Label.textColor = MarvelStyles.secondary.white
        
        filter1Switch.onTintColor = MarvelStyles.primary.marvel
        filter2Switch.onTintColor = MarvelStyles.primary.marvel
    }
    
    private func searchData(key: String) {
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
        
        let searchHasChanged: Bool = presenter?.searchHasChanged(isFilter1Enabled: filter1Switch.isOn, isFilter2Enabled: filter2Switch.isOn, searchKey: key) ?? false
        
        presenter?.searchItems(isFilter1Enabled: filter1Switch.isOn, isFilter2Enabled: filter2Switch.isOn, searchKey: key, result: { [weak self] result in
            guard let self = self else { return }
            
            switch result {
                case .success(let items):
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.isHidden = true
                    self.emptyView.isHidden = !items.isEmpty
                    self.dataSource.setData(items)
                    
                    if searchHasChanged {
                        self.collectionView.setContentOffset(.zero, animated: true)
                    }
                }
                
                case .failure(let error):
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.isHidden = true
                }
                print(error.localizedDescription)
            }
        })
    }
}

extension SearchViewController: VerticalCollectionViewDelegate {
    func didSelectItem(item: HomeItem, collectionView: UICollectionView) {
        guard let navigation = navigationController else { return }
        presenter?.showItemDetails(navigation: navigation, item: item)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       if(collectionView.contentOffset.y >= (collectionView.contentSize.height - collectionView.bounds.size.height)) {
           
           //Don't get new items if the API is still loading
           guard !self.activityIndicator.isAnimating else { return }
           
           //Verifies if the pagination is on the limit
           guard !(presenter?.isPaginationLimit() ?? false) else { return }
           searchItems()
       }
   }
}

extension SearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard searchText.count % 2 == 0 else { return }
        
        searchItems()
    }
    
    func searchItems() {
        guard let searchText: String = searchBar.text else { return }
        searchData(key: searchText)
    }
}

//Switchs
private extension SearchViewController {
    @IBAction func filter1ValueChanged(_ sender: Any) {
        filter2Switch.isOn = !filter1Switch.isOn
        searchItems()
    }
    
    @IBAction func filter2ValueChanged(_ sender: Any) {
        filter1Switch.isOn = !filter2Switch.isOn
        searchItems()
    }
}
