//
//  VerticalCollectionViewDataSource.swift
//  marvel
//
//  Created by Emiliano Da Luz on 24/01/2022.
//

import Foundation
import UIKit

protocol VerticalCollectionViewDataSourceProtocol {
    var delegate: VerticalCollectionViewDelegate? { get set }
    func setData(_ items: [HomeItem])
}

protocol VerticalCollectionViewDelegate {
    func didSelectItem(item: HomeItem, collectionView: UICollectionView)
    func scrollViewDidScroll(_ scrollView: UIScrollView)
}

class VerticalCollectionViewDataSource: NSObject, VerticalCollectionViewDataSourceProtocol {
    
    private enum Constant {
        static let cellHeight: CGFloat = 200
        static let cellsSpacing: CGFloat = 12
        static let horizontalSpacing: CGFloat = 24
    }
    
    var delegate: VerticalCollectionViewDelegate?
    private var items: [HomeItem] = []
    
    weak var collectionView: UICollectionView? {
        didSet {
            setupCollectionView()
        }
    }
    
    func setData(_ items: [HomeItem]) {
        self.items = items
        self.collectionView?.reloadData()
    }
    
    private func setupCollectionView() {
        collectionView?.register(UINib.init(nibName: String(describing: VerticalCollectionViewCell.self), bundle: Bundle.main), forCellWithReuseIdentifier: String(describing: VerticalCollectionViewCell.self))
        
        collectionView?.dataSource = self
        collectionView?.delegate = self
    }
}

extension VerticalCollectionViewDataSource: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: VerticalCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: VerticalCollectionViewCell.self), for: indexPath) as? VerticalCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        //Avoid out of index if it's updating info while new data has been set
        guard indexPath.row < items.count else { return cell }
        
        let item = items[indexPath.row]
        cell.setupCell(item: item)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        delegate?.didSelectItem(item: item, collectionView: collectionView)
    }
}

extension VerticalCollectionViewDataSource: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width - Constant.horizontalSpacing, height: Constant.cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return Constant.cellsSpacing
    }
}

extension VerticalCollectionViewDataSource: UICollectionViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        delegate?.scrollViewDidScroll(scrollView)
   }
}

