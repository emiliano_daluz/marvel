//
//  VerticalCollectionViewCell.swift
//  marvel
//
//  Created by Emiliano Da Luz on 25/01/2022.
//

import UIKit

class VerticalCollectionViewCell: UICollectionViewCell {

    private enum Constant {
        static let titleNumberOfLines: Int = 2
        static let imageNotAvailableKey: String = "image_not_available"
        static let placeholder: UIImage = UIImage(named: "placeholder") ?? UIImage()
    }
    
    @IBOutlet private var imageView: UIImageView!
    
    @IBOutlet private var titleLabel: UILabel! {
        didSet {
            titleLabel.numberOfLines = Constant.titleNumberOfLines
            titleLabel.font = MarvelStyles.font.headlineBold
            titleLabel.textColor = MarvelStyles.secondary.black
        }
    }
    
    @IBOutlet private var subtitleLabel: UILabel! {
        didSet {
            subtitleLabel.font = MarvelStyles.font.captionOneSemiBold
            subtitleLabel.textColor = MarvelStyles.secondary.black
        }
    }
        
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        layer.cornerRadius = 16.0
        clipsToBounds = true
    }
    
    func setupCell(item: HomeItem) {
        //Split the character name by '(' to take the first and second name from the Character's name
        //TODO: move from backend side the second name to another field removing '()'
        let names = item.title?.split(separator: "(")
        titleLabel.text = names?.first?.description.uppercased() ?? item.title?.uppercased()
        
        if names?.count ?? 0 > 1, let subtitle =  names?[1].description {
            subtitleLabel.text = subtitle.replacingOccurrences(of: ")", with: "")
        } else {
            subtitleLabel.text = ""
        }
                        
        guard let thumbnail = item.thumbnail, let imageURL = ImageHelper.getImageURL(thumbnail: thumbnail) else { return }
                
        imageView.loadImage(at: imageURL, placeholder: Constant.placeholder)
        
        //When the image is not available, the aspect ratio is changed to show to the user the message inside the image
        imageView.contentMode = imageURL.absoluteString.contains(Constant.imageNotAvailableKey) ? .scaleToFill : .scaleAspectFill
    }
    
    override func prepareForReuse() {
        imageView.image = nil
        imageView.cancelImageLoad()
        super.prepareForReuse()
    }
}
