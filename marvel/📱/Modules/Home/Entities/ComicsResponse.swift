//
//  ComicsResponse.swift
//  marvel
//
//  Created by Emiliano Da Luz on 23/01/2022.
//

import Foundation

struct Comic: HomeItem {
    var id: Int?
    var title: String?
    var description: String?
    var thumbnail: Thumbnail?
}
