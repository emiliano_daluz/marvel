//
//  CharactersResponse.swift
//  marvel
//
//  Created by Emiliano Da Luz on 22/01/2022.
//

import Foundation

struct Character: HomeItem {
    var id: Int?
    var title: String?
    var description: String?
    var thumbnail: Thumbnail?
    var comics: CharacterComic?
    
    enum CodingKeys: String, CodingKey {
        case id
        case title = "name"
        case description
        case thumbnail
        case comics
    }
}

struct CharacterComic: Decodable {
    let available: Int?
    let collectionURI: String?
}

struct Thumbnail: Decodable {
    let path: String?
    let format: String?
    
    enum CodingKeys: String, CodingKey {
        case path
        //this coding key was changed because the word "extension" is a Swift keyword
        case format = "extension"
    }
}
