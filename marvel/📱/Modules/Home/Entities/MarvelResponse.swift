//
//  MarvelResponse.swift
//  marvel
//
//  Created by Emiliano Da Luz on 23/01/2022.
//

import Foundation

struct MarvelResponse<T: Decodable>: Decodable {
    let attributionText: String?
    let data: MarvelData<T>?
}

struct MarvelData<T: Decodable>: Pagination, Decodable {
    var offset: Int?
    var limit: Int?
    var total: Int?
    var count: Int?
    let results: [T]?
}

protocol Pagination: Decodable {
    var offset: Int? { get set }
    var limit: Int? { get set }
    var total: Int? { get set }
    var count: Int? { get set }
}

protocol HomeItem: Decodable {
    var id: Int? { get set }
    var title: String? { get set }
    var description: String? { get set }
    var thumbnail: Thumbnail? { get set }
}
