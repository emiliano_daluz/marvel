//
//  HorizontalCollectionViewCell.swift
//  marvel
//
//  Created by Emiliano Da Luz on 23/01/2022.
//

import UIKit
import Foundation

class HorizontalCollectionViewCell: UICollectionViewCell {

    private enum Constant {
        static let titleNumberOfLines: Int = 2
        static let imageNotAvailableKey: String = "image_not_available"
        static let placeholder: UIImage = UIImage(named: "placeholder") ?? UIImage()
    }
    
    @IBOutlet private var imageView: UIImageView!
    @IBOutlet private var titleLabel: UILabel! {
        didSet {
            titleLabel.numberOfLines = Constant.titleNumberOfLines
            titleLabel.font = MarvelStyles.font.bodyBold
            titleLabel.textColor = MarvelStyles.secondary.white
        }
    }
    
    @IBOutlet private var subtitleLabel: UILabel! {
        didSet {
            subtitleLabel.font = MarvelStyles.font.captionOneSemiBold
            subtitleLabel.textColor = MarvelStyles.secondary.lightGray
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = MarvelStyles.secondary.black
    }
    
    func setupCell(item: HomeItem) {
        //Split the character name by '(' to take the first and second name from the Character's name
        //TODO: move from backend side the second name to another field removing '()'
        let names = item.title?.split(separator: "(")
        titleLabel.text = names?.first?.description.uppercased() ?? item.title?.uppercased()
        
        if names?.count ?? 0 > 1, let subtitle =  names?[1].description {
            subtitleLabel.text = subtitle.replacingOccurrences(of: ")", with: "")
        } else {
            subtitleLabel.text = ""
        }
        
        //Set image
        guard let thumbnail = item.thumbnail, let imageURL = ImageHelper.getImageURL(thumbnail: thumbnail) else { return }
        imageView.loadImage(at: imageURL, placeholder: Constant.placeholder)
        
        //When the image is not available, the aspect ratio is changed to show to the user the message inside the image
        imageView.contentMode = imageURL.absoluteString.contains(Constant.imageNotAvailableKey) ? .scaleToFill : .scaleAspectFill
    }
    
    override func prepareForReuse() {
        imageView.image = nil
        imageView.cancelImageLoad()
        super.prepareForReuse()
    }
}
