//
//  HomeViewController.swift
//  marvel
//
//  Created by Emiliano Da Luz on 22/01/2022.
//

import Foundation
import UIKit

class HomeViewController: UIViewController {
    
    private enum Constant {
        static let title: String = "Home"
    }
        
    @IBOutlet private var charactersContainerView: UIView!
    @IBOutlet private var comicsContainerView: UIView!
        
    private var charactersList: HorizontalCollectionView = HorizontalCollectionView()
    private var comicsList: HorizontalCollectionView = HorizontalCollectionView()
    
    private var presenter: HomePresenterTestable?
    
    //MARK: Init
    init(with presenter: HomePresenterTestable) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    //MARK: Lyfe Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        loadData()
    }
}

private extension HomeViewController {
    func setupViews() {
        title = Constant.title
        
        //Characters List
        charactersList.delegate = self
        charactersList.translatesAutoresizingMaskIntoConstraints = false
        charactersContainerView.addSubview(charactersList)
        charactersList.topAnchor.constraint(equalTo: charactersContainerView.topAnchor).isActive = true
        charactersList.leadingAnchor.constraint(equalTo: charactersContainerView.leadingAnchor).isActive = true
        charactersList.trailingAnchor.constraint(equalTo: charactersContainerView.trailingAnchor).isActive = true
        charactersList.bottomAnchor.constraint(equalTo: charactersContainerView.bottomAnchor).isActive = true
        
        //Comics List
        comicsList.delegate = self
        comicsList.translatesAutoresizingMaskIntoConstraints = false
        comicsContainerView.addSubview(comicsList)
        comicsList.topAnchor.constraint(equalTo: comicsContainerView.topAnchor).isActive = true
        comicsList.leadingAnchor.constraint(equalTo: comicsContainerView.leadingAnchor).isActive = true
        comicsList.trailingAnchor.constraint(equalTo: comicsContainerView.trailingAnchor).isActive = true
        comicsList.bottomAnchor.constraint(equalTo: comicsContainerView.bottomAnchor).isActive = true
    }
    
    //MARK: Load Data
    func loadData() {
        loadCharacters()
        loadComics()
    }
    
    func loadCharacters() {
        charactersList.startAnimating()
        
        presenter?.getCharacters(result: { [weak self] result in
            guard let self = self else { return }
            switch result {
                case .success(let response):
                guard let characters = response.data?.results else { return }
                
                DispatchQueue.main.async {
                    self.charactersList.stopAnimating()
                    self.charactersContainerView.isHidden = false
                    self.charactersList.title = LanguageString.characterList + " >"
                    self.charactersList.setData(characters)
                }
                
                case .failure(_):
                    DispatchQueue.main.async {
                        self.charactersList.stopAnimating()
                        self.charactersList.showEmptyView()
                    }
            }
        })
    }
    
    func loadComics() {
        comicsList.startAnimating()
        
        presenter?.getComics(result: { [weak self] result in
            guard let self = self else { return }
            switch result {
                case .success(let response):
                guard let comics = response.data?.results else { return }
                
                DispatchQueue.main.async {
                    self.comicsList.stopAnimating()
                    self.comicsList.title = LanguageString.marvelComics + " >"
                    self.comicsList.setData(comics)
                }
                case .failure(_):
                    DispatchQueue.main.async {
                        self.comicsList.stopAnimating()
                        self.comicsList.showEmptyView()
                    }
            }
        })
    }
}

//Horizontal List Delegate
extension HomeViewController: HorizontalCollectionViewDelegate {
    func didSelectItem(item: HomeItem) {
        guard let navigation = navigationController else { return }
        presenter?.showItemDetails(navigation: navigation, item: item)
    }
    
    func headerTapped(horizontalCollectionView: HorizontalCollectionView) {
        guard let navigation = navigationController else { return }
        
        if horizontalCollectionView == charactersList {
            presenter?.showSearch(navigation: navigation, itemType: .character)
        } else {
            presenter?.showSearch(navigation: navigation, itemType: .comic)
        }
    }
    
    func retryButtonTapped(horizontalCollectionView: HorizontalCollectionView) {
        horizontalCollectionView == charactersList ? loadCharacters() : loadComics()
    }
}
