//
//  HorizontalCollectionView.swift
//  marvel
//
//  Created by Emiliano Da Luz on 26/01/2022.
//

import Foundation
import UIKit

class HorizontalListLayout: UICollectionViewFlowLayout {
    
    fileprivate enum Layout {
        static let minimumLineSpacing: CGFloat = 12
        static let minimumInteritemSpacing: CGFloat = 12
        static let baseSectionInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    override init() {
        super.init()
        setup()
    }
    
    fileprivate func setup() {
        scrollDirection = .horizontal
        minimumLineSpacing = minimumLineSpacing
        minimumInteritemSpacing = minimumInteritemSpacing
        sectionInset = Layout.baseSectionInsets
    }
}

protocol HorizontalCollectionViewDelegate {
    func didSelectItem(item: HomeItem)
    func headerTapped(horizontalCollectionView: HorizontalCollectionView)
    func retryButtonTapped(horizontalCollectionView: HorizontalCollectionView)
}

class HorizontalCollectionView: UIView {
    
    private enum Constant {
        static let verticalSpacing: CGFloat = 16
        static let horizontalSpacing: CGFloat = 16
    }
    
    private let emptyView: HorizontalEmptyStateView = HorizontalEmptyStateView()
    private let skeletonView: HorizontalCollectionViewSkeleton = HorizontalCollectionViewSkeleton()
    private let titleLabel: UILabel = UILabel()
    private let collectionView: UICollectionView = UICollectionView(frame: .zero, collectionViewLayout: HorizontalListLayout())
    
    var delegate: HorizontalCollectionViewDelegate?
    var title: String = "" {
        didSet {
            titleLabel.text = title
        }
    }
    
    private var dataSource: HorizontalCollectionViewDataSource = HorizontalCollectionViewDataSource()
    
    init() {
        super.init(frame: .zero)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupSubviews()
    }
    
    func setData(_ items: [HomeItem]) {
        dataSource.setData(items)
    }
    
    func startAnimating() {
        titleLabel.isHidden = true
        collectionView.isHidden = true
        emptyView.isHidden = true
        skeletonView.isHidden = false
    }
    
    func stopAnimating() {
        titleLabel.isHidden = false
        collectionView.isHidden = false
        emptyView.isHidden = true
        skeletonView.isHidden = true
    }
    
    func showEmptyView() {
        emptyView.isHidden = false
        titleLabel.isHidden = true
        collectionView.isHidden = true
        skeletonView.isHidden = true
    }
    
    private func setupSubviews() {
        backgroundColor = MarvelStyles.secondary.white
        collectionView.backgroundColor = MarvelStyles.secondary.white
        dataSource.collectionView = collectionView
        
        //Title Label
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(titleLabelTapped))
        titleLabel.isUserInteractionEnabled = true
        titleLabel.addGestureRecognizer(tapGesture)
        titleLabel.font = MarvelStyles.font.headlineBold
        titleLabel.textColor = MarvelStyles.secondary.black
        
        addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        titleLabel.topAnchor.constraint(equalTo: topAnchor).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constant.horizontalSpacing).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constant.horizontalSpacing).isActive = true
        
        //Collection View
        addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        collectionView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Constant.verticalSpacing).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constant.horizontalSpacing).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constant.horizontalSpacing).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        //Data Source
        dataSource.delegate = self
        
        //Skeleton
        skeletonView.isHidden = true
        addSubview(skeletonView)
        skeletonView.translatesAutoresizingMaskIntoConstraints = false
        
        skeletonView.topAnchor.constraint(equalTo: topAnchor, constant: Constant.verticalSpacing).isActive = true
        skeletonView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        skeletonView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        skeletonView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        //Empty View
        emptyView.delegate = self
        emptyView.isHidden = true
        addSubview(emptyView)
        emptyView.translatesAutoresizingMaskIntoConstraints = false
        
        emptyView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        emptyView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constant.horizontalSpacing).isActive = true
        emptyView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constant.horizontalSpacing).isActive = true
        emptyView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    @objc func titleLabelTapped(sender: UITapGestureRecognizer) {
        delegate?.headerTapped(horizontalCollectionView: self)
    }
}

extension HorizontalCollectionView: HorizontalDataSourceDelegate {
    func didSelectItem(item: HomeItem) {
        delegate?.didSelectItem(item: item)
    }
}

extension HorizontalCollectionView: HorizontalEmptyStateViewDelegate {
    func retryButtonTapped() {
        delegate?.retryButtonTapped(horizontalCollectionView: self)
    }
}

