//
//  HorizontalEmptyStateView.swift
//  marvel
//
//  Created by Emiliano Da Luz on 26/01/2022.
//

import Foundation
import UIKit

protocol HorizontalEmptyStateViewDelegate {
    func retryButtonTapped()
}

class HorizontalEmptyStateView: UIView {
    
    private enum Constant {
        static let imageTopSpacing: CGFloat = 32
        static let verticalSpacing: CGFloat = 16
        static let horizontalSpacing: CGFloat = 16
        static let image = UIImage(named: "spiderMan")
        static let imageSize: CGFloat = 150
        static let titleNumberOfLines: Int = 2
    }
    
    var delegate: HorizontalEmptyStateViewDelegate?
    
    init() {
        super.init(frame: .zero)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupSubviews()
    }
    
    private func setupSubviews() {
        backgroundColor = MarvelStyles.secondary.black
        
        //Image View
        let imageView: UIImageView = UIImageView()
        imageView.image = Constant.image
        
        addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        imageView.topAnchor.constraint(equalTo: topAnchor, constant: Constant.imageTopSpacing).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: Constant.imageSize).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: Constant.imageSize).isActive = true
        imageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        //Title View
        let titleLabel: UILabel = UILabel()
        titleLabel.font = MarvelStyles.font.headlineBold
        titleLabel.textColor = MarvelStyles.secondary.white
        titleLabel.numberOfLines = Constant.titleNumberOfLines
        titleLabel.textAlignment = .center
        titleLabel.text = LanguageString.somethingWentWrong
        
        addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        titleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: Constant.verticalSpacing).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constant.horizontalSpacing).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constant.horizontalSpacing).isActive = true
    
        //Collection View
        let retryButton: UIButton = UIButton()
        retryButton.addTarget(self, action: #selector(retryButtonTapped), for: .touchUpInside)
        retryButton.backgroundColor = MarvelStyles.primary.marvel
        retryButton.setTitle(LanguageString.retry, for: .normal)
        retryButton.layer.cornerRadius = 8
        retryButton.clipsToBounds = true
        
        addSubview(retryButton)
        retryButton.translatesAutoresizingMaskIntoConstraints = false
        
        retryButton.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Constant.verticalSpacing).isActive = true
        retryButton.centerXAnchor.constraint(equalTo: titleLabel.centerXAnchor).isActive = true
        retryButton.widthAnchor.constraint(equalToConstant: Constant.imageSize).isActive = true
        
    }
    
    @objc private func retryButtonTapped() {
        delegate?.retryButtonTapped()
    }
}
