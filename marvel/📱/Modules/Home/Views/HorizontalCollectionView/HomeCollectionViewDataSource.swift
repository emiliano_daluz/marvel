//
//  HomeCollectionViewDataSource.swift
//  marvel
//
//  Created by Emiliano Da Luz on 23/01/2022.
//

import Foundation
import UIKit

protocol HorizontalDataSourceProtocol {
    var delegate: HorizontalDataSourceDelegate? { get set }
    func setData(_ items: [HomeItem])
}

protocol HorizontalDataSourceDelegate {
    func didSelectItem(item: HomeItem)
}

class HorizontalCollectionViewDataSource: NSObject, HorizontalDataSourceProtocol, UICollectionViewDelegate {
    
    private enum Constant {
        static let cornerRadius: CGFloat = 16
    }
    
    var delegate: HorizontalDataSourceDelegate?
    private var items: [HomeItem] = []
    
    weak var collectionView: UICollectionView? {
        didSet {
            setupCollectionView()
        }
    }
    
    func setData(_ items: [HomeItem]) {
        self.items = items
        self.collectionView?.reloadData()
    }
    
    private func setupCollectionView() {
        collectionView?.register(UINib.init(nibName: String(describing: HorizontalCollectionViewCell.self), bundle: Bundle.main), forCellWithReuseIdentifier: String(describing: HorizontalCollectionViewCell.self))
        
        collectionView?.dataSource = self
        collectionView?.delegate = self
    }
}

extension HorizontalCollectionViewDataSource: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: HorizontalCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: HorizontalCollectionViewCell.self), for: indexPath) as? HorizontalCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        cell.setupCell(item: items[indexPath.row])
        cell.layer.cornerRadius = Constant.cornerRadius
        cell.clipsToBounds = true
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        delegate?.didSelectItem(item: item)
    }
}

extension HorizontalCollectionViewDataSource: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width / 3, height: collectionView.frame.size.height - Constant.cornerRadius)
    }
}
