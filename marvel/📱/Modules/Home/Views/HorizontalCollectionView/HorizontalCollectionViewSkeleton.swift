//
//  HorizontalCollectionViewSkeleton.swift
//  marvel
//
//  Created by Emiliano Da Luz on 26/01/2022.
//

import Foundation
import UIKit

class HorizontalCollectionViewSkeleton: UIView {
    
    private enum Constant {
        static let verticalSpacing: CGFloat = 16
        static let horizontalSpacing: CGFloat = 16
        static let titleHeight: CGFloat = 20
    }
    
    init() {
        super.init(frame: .zero)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupSubviews()
    }
    
    private func setupSubviews() {
        //Title View
        let titleViewSkeleton: UIView = UIView()
        titleViewSkeleton.backgroundColor = MarvelStyles.secondary.lightGray
        
        addSubview(titleViewSkeleton)
        titleViewSkeleton.translatesAutoresizingMaskIntoConstraints = false
        
        titleViewSkeleton.topAnchor.constraint(equalTo: topAnchor).isActive = true
        titleViewSkeleton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constant.horizontalSpacing).isActive = true
        titleViewSkeleton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constant.horizontalSpacing).isActive = true
        titleViewSkeleton.heightAnchor.constraint(equalToConstant: Constant.titleHeight).isActive = true
        
        //Collection View
        let collectionViewSkeleton: UIView = UIView()
        collectionViewSkeleton.backgroundColor = MarvelStyles.secondary.lightGray
        
        addSubview(collectionViewSkeleton)
        collectionViewSkeleton.translatesAutoresizingMaskIntoConstraints = false
        
        collectionViewSkeleton.topAnchor.constraint(equalTo: titleViewSkeleton.bottomAnchor, constant: Constant.verticalSpacing).isActive = true
        collectionViewSkeleton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constant.horizontalSpacing).isActive = true
        collectionViewSkeleton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constant.horizontalSpacing).isActive = true
        collectionViewSkeleton.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
}
