//
//  HomeWireframe.swift
//  marvel
//
//  Created by Emiliano Da Luz on 22/01/2022.
//

import Foundation
import UIKit

protocol HomeWireframeTestable {
    func showHome(navigation: UINavigationController, presenter: HomePresenterTestable)
    func showItemDetails(navigation: UINavigationController, item: HomeItem)
    func showSearch(navigation: UINavigationController, itemType: MarvelItem)
}

class HomeWireframe: HomeWireframeTestable {
    func showHome(navigation: UINavigationController, presenter: HomePresenterTestable) {
        let homeVC = HomeViewController(with: presenter)
        navigation.setViewControllers([homeVC], animated: false)
    }
    
    func showItemDetails(navigation: UINavigationController, item: HomeItem) {
        let itemDetails = ItemDetailsModule(item: item)
        itemDetails.showItemDetails(navigation: navigation)
    }
    
    func showSearch(navigation: UINavigationController, itemType: MarvelItem) {
        let search = SearchModule(itemType: itemType)
        search.showSearch(navigation: navigation)
    }
}
