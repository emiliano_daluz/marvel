//
//  HomeProvider.swift
//  marvel
//
//  Created by Emiliano Da Luz on 22/01/2022.
//

import Foundation

protocol HomeProviderProtocol {
    func getCharacters(result: @escaping (Result<MarvelResponse<Character>, APIServiceError>) -> Void)
    func getComics(result: @escaping (Result<MarvelResponse<Comic>, APIServiceError>) -> Void)
}

class HomeProvider: HomeProviderProtocol {
    
    private enum Domain {
        static let getCharacters = MarvelAPIClient.baseURL + "/characters"
        static let getComics = MarvelAPIClient.baseURL + "/comics"
    }
    
    func getCharacters(result: @escaping (Result<MarvelResponse<Character>, APIServiceError>) -> Void) {
        guard let url = URL(string: Domain.getCharacters) else { return }
       return MarvelAPIClient.fetchResources(url: url, completion: result)
    }
    
    func getComics(result: @escaping (Result<MarvelResponse<Comic>, APIServiceError>) -> Void) {
        guard let url = URL(string: Domain.getComics) else { return }
       return MarvelAPIClient.fetchResources(url: url, completion: result)
    }
}
