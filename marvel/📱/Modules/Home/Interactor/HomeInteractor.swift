//
//  HomeInteractor.swift
//  marvel
//
//  Created by Emiliano Da Luz on 22/01/2022.
//

import Foundation

protocol HomeInteractorTestable {
    func getCharacters(result: @escaping (Result<MarvelResponse<Character>, APIServiceError>) -> Void)
    func getComics(result: @escaping (Result<MarvelResponse<Comic>, APIServiceError>) -> Void)
}

class HomeInteractor: HomeInteractorTestable {
    private var provider: HomeProviderProtocol

    init(provider: HomeProviderProtocol = HomeProvider()) {
        self.provider = provider
    }
    
    func getCharacters(result: @escaping (Result<MarvelResponse<Character>, APIServiceError>) -> Void) {
        provider.getCharacters { response in
            switch response {
            case .success(let responseData):
                result(.success(responseData))
            case .failure(let error):
                //Mock the home when service call fails
                guard let response: MarvelResponse<Character> = TestUtils.parseJSON("getCharactersMock.json") else {
                    result(.failure(error))
                    return
                }
                
                result(.success(response))
            }
        }
    }
    
    func getComics(result: @escaping (Result<MarvelResponse<Comic>, APIServiceError>) -> Void) {
        //TODO: Mock the home when service call fails for this case
        ///don't use the mock to show empty view for collecction view
        return provider.getComics(result: result)
    }
}
