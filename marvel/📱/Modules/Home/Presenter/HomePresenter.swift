//
//  HomePresenter.swift
//  marvel
//
//  Created by Emiliano Da Luz on 22/01/2022.
//

import Foundation
import UIKit

protocol HomePresenterTestable {
    var wireframe: HomeWireframeTestable { get }
    var interactor: HomeInteractorTestable { get }
    
    func showHome(navigation: UINavigationController)
    func showItemDetails(navigation: UINavigationController, item: HomeItem)
    func showSearch(navigation: UINavigationController, itemType: MarvelItem)
    func getCharacters(result: @escaping (Result<MarvelResponse<Character>, APIServiceError>) -> Void)
    func getComics(result: @escaping (Result<MarvelResponse<Comic>, APIServiceError>) -> Void)
}

//Add more items here from Marvel Backend API to handling as the same way of these Marvel Items
enum MarvelItem {
    case character
    case comic
}

class HomePresenter: HomePresenterTestable {
    var wireframe: HomeWireframeTestable
    var interactor: HomeInteractorTestable
    
    init(wireframe: HomeWireframeTestable, interactor: HomeInteractorTestable) {
        self.wireframe = wireframe
        self.interactor = interactor
    }
    
    //Transitions
    func showHome(navigation: UINavigationController) {
        wireframe.showHome(navigation: navigation, presenter: self)
    }
    
    func showItemDetails(navigation: UINavigationController, item: HomeItem) {
        wireframe.showItemDetails(navigation: navigation, item: item)
    }
    
    func showSearch(navigation: UINavigationController, itemType: MarvelItem) {
        wireframe.showSearch(navigation: navigation, itemType: itemType)
    }
    
    //Backend Data
    func getCharacters(result: @escaping (Result<MarvelResponse<Character>, APIServiceError>) -> Void) {
        return interactor.getCharacters(result: result)
    }
    
    func getComics(result: @escaping (Result<MarvelResponse<Comic>, APIServiceError>) -> Void) {
        return interactor.getComics(result: result)
    }
}
