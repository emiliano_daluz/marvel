//
//  HomeModule.swift
//  marvel
//
//  Created by Emiliano Da Luz on 22/01/2022.
//

import Foundation
import UIKit

class HomeModule {
    private let presenter: HomePresenterTestable
    private let wireframe: HomeWireframeTestable
    private let interactor: HomeInteractorTestable
    
    init() {
        self.wireframe = HomeWireframe()
        self.interactor = HomeInteractor()
        self.presenter = HomePresenter(wireframe: self.wireframe, interactor: self.interactor)
    }
    
    func showHome(navigation: UINavigationController) {
        presenter.showHome(navigation: navigation)
    }
}
