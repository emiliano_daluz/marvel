//
//  LanguageKey.swift
//  marvel
//
//  Created by Emiliano Da Luz on 24/01/2022.
//

import Foundation

enum LanguageKey: String {
    //Home
    case characterList
    case marvelComics
    
    //Item Details
    case emptyDescription
    case character
    case comic
    case description
    
    //Empty View
    case emptyViewTitle
    
    //Error
    case somethingWentWrong
    case retry
    
    //Search
    case name
    case title
    case modified
    case publicationDate
    
    func localize() -> String {
        return NSLocalizedString(self.rawValue, comment: "")
    }
}

