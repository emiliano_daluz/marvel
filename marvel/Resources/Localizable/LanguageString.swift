//
//  LanguageString.swift
//  marvel
//
//  Created by Emiliano Da Luz on 24/01/2022.
//

import Foundation

class LanguageString: NSObject {
    //Home
    static var characterList: String { return LanguageKey.characterList.localize() }
    static var marvelComics: String { return LanguageKey.marvelComics.localize() }
    
    //Item Details
    static var emptyDescription: String { return LanguageKey.emptyDescription.localize() }
    static var character: String { return LanguageKey.character.localize() }
    static var comic: String { return LanguageKey.comic.localize() }
    static var description: String { return LanguageKey.description.localize() }
    
    //Error
    static var somethingWentWrong: String { return LanguageKey.somethingWentWrong.localize() }
    static var retry: String { return LanguageKey.retry.localize() }
    
    //Empty View
    static var emptyViewTitle: String { return LanguageKey.emptyViewTitle.localize() }
    
    //Search
    static var name: String { return LanguageKey.name.localize() }
    static var title: String { return LanguageKey.title.localize() }
    static var modified: String { return LanguageKey.modified.localize() }
    static var publicationDate: String { return LanguageKey.publicationDate.localize() }
}

