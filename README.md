* * *

# MARVEL Guidelines and Documentation

* [Code Style guide](https://github.com/tak3mat3k/swift-style-guide)
* [Good practices](https://github.com/tak3mat3k/swift-style-guide)

* * *

# Architecture Documentation

## VIPER Components


* [Module]
* [Wireframe]
* [Presenter]
* [Interactor]
* [Entities]
* [View (UIViewController)]

* * *

### Module

**Responsabilities:** This class is responsable for module components creation. `Wireframe`, `Presenter`, `Interactor`.

> *Be careful this class cannot reference the Interactor and `Wireframe`, it only needs the `Presenter` reference to send actions if it's needed*.

**What don't do:** 
* This class should not save `Interactor` or `Wireframe` references and less try to send messages to them. 
* don't use singletons to save this reference.

* * *

### Wireframe

**Responsabilities:** This class is responsable for module navigation. What it means, this class have the reference to Navigation container whatever it is `Navigation Controller`, `Tabbar Controller`, `Drawer Controller`, etc.

**What don't do:** 
* Don't reference the `Presenter` or `Interactor` never.
* Don't save view controllers references.

This class always must implement `RPBaseWireframe` protocol:

````
#!swift
protocol RPBaseWireframe : class {

    associatedtype Transition
    var baseController: UIViewController { get }
    var animator: WireframeAnimator { get set }

    func performTransition(transition: Transition, onCompletion completion: RPCompletionBlock?) throws
}		
````
* * *

### Presenter

**Responsabilities:** The presenter is the bridge between all VIPER components, the presenter owns the Interactor and `Wireframe` and it sends events to them, currently there isn't a protocol to define a presenter, this is defined for each module.

**What don't do:** 
* Don't perform any kind of logic here.
* Don't save view controllers reference.
* Don't perform transitions here.

This component is defined by a protocol, but not only one actually should exists a presenter protocol for each view controller like this:

````
#!swift
protocol ViewControllerOnePresenter : class {
    //methods and properties expose to ViewControllerOne goes here
}	

protocol ViewControllerTwoPresenter : class {
    //methods and properties expose to ViewControllerTwo goes here
}	

//The presenter class implements both protocols but each one of those only knows about its presenter protocol
class ModulePresenter {
    //presenter properties and methods
}

//MARK: - ViewControllerOnePresenter
extension ModulePresenter : ViewControllerOnePresenter {
    //ViewControllerOnePresenter implementations goes here
}

//MARK: - ViewControllerTwoPresenter
extension ModulePresenter : ViewControllerTwoPresenter {
    //ViewControllerTwoPresenter implementations goes here
}

````
* * *

### Interactor

**Responsabilities:** This class is responsable for module logic, Requests, Persistence, Data etc. This class has the all logic about the module.

This component is defined by a protocol and can only be referenced by the `Presenter`, This class has no references to VIPER components it is `Self-sufficient`.

The communication with the presenter is through callbacks, delegate or RxSwift.

**What don't do:** 
* Don't reference the `Presenter` or `Wireframe` never.

* * *

### Entities

The entities are the models necessaries to work with this module, classes, structs, enums, etc.

### View (ViewController)

The view controller are clear enough but there are some topics to care about:

* View Controllers can only communicate with its Presenter no more.
* View Controllers never never know about other views.
* View Controllers don't have any kind of logic, its responsability is show the data and that's it.

Small example of view controller:

````
#!swift
class ExampleViewController: UIViewController {

    //IBOutlets goes here

    //Always create your own init with Presenter parameter mandatory.
    init(presenter: ExampleViewControllerPresenter) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
}	
````

* * *

# VIPER Diagram

![Alt Viper Diagram](https://medium.com/cr8resume/viper-architecture-for-ios-project-with-simple-demo-example-7a07321dbd29)

# VIPER Rules

* `ViewController` owns a `Presenter`.
* `Presenter` owns a `Interactor` and `Wireframe`.
* `Wireframe` and `Interactor` **never never never never** must have `Presenter` reference **(NEVER)**.
* `Wireframe` must not save *viewController* references.
* `Wireframe` and `Presenter` communication should be with subjects, ALWAYS not exception. This rule apply when communication is with RxSwift.
* `Wireframe` transitions should be called through the `Presenter`.
* `Interactor` and `Presenter` communication should be with subjects too, ALWAYS not exception. This rule apply when communication is with RxSwift.
* `ViewController` can only send events to its `Presenter` never place something stupid like subjects within it.
* The modules shouldn't be singletons if you are gonna use a module always create a new one.
* Nobody else should call `Wireframe` or `Interactor` methods only the `Presenter` can perform those actions.


# User Interface

* Always test the UI in a real device, to detect any issue related to animations
* Remember to localize all copies used in all screen, an test your UI in all different supported languages
* Do not make calculation using the text length, because the same copy will have a different length in a different language
* Do not hardcoded int or float values (in code, .xibs, .storyboard) to calculate the size of UI elements, there are several UIView extensions to help you make calculations proportional to screen dimentions
* Always add accesibility ids by code to all relavant UI elements, to allow creation of Functional Automated Test cases. For instance UILabels, UIButtons, UITableViewCells, UIPickers, etc
* Make sure your design works in iPad too

# Language:

* The app is localized using the Localizable.strings method, if you are going to add any label, please assign an unique identifier to it, and add it to:
    1. `LanguageKey.swift`
    2. `LanguageString.swift`
    3. `Localizable.strings (English)`
    4. `Localizable.strings (Spanish)`
* Then you can request the right copy, according to device language, just by calling `LanguageString.copyName`, where **copyName** is the unique identifier previously assigned
* Remember that `Localizable.strings` file requires a semicolon at the end of any entry, if you forget to add it, an error will be shown but Xcode will not show you the line...
* Before trying to create a new identifier, please check if there is already a copy you can use that suits your needs

* * *
